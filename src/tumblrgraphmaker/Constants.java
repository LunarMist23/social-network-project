package tumblrgraphmaker;

import java.text.SimpleDateFormat;

public class Constants {
	
	public static final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
	public static final SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMDDhhmmssSSS");
	
	public static final int maxPostLoop = 20;
	
	/**
	 * Must be a multiple of 20
	 */
	public static final int postUpdateIncrement = 1000;
	
	/**
	 * API Key from Tumblr Application API
	 */
	public static final String authKey = "bBkfS8KqjO5ltw9EQWazH4L38sjAIjkvASE45IXA9bXT1aXz4W";
	
	/**
	 * URL to request blog info. Use with String.format, and pass two Strings, the blog name and Constants.authKey
	 */
	public static final String blogInfo = "http://api.tumblr.com/v2/blog/%s.tumblr.com/info?api_key=%s";
	
	/**
	 * URL to request posts. Use with String.format, and pass two strings and an integer - blog name, Constants.authKey, and then the post offset.
	 */
	public static final String posts = "http://api.tumblr.com/v2/blog/%s.tumblr.com/posts?api_key=%s&reblog_info=true&offset=%d";
	
	public static final int maxThreads = 25;
	
	public static final int defaultDepth = 2;
	
	public static final int defaultYear = 1;
}

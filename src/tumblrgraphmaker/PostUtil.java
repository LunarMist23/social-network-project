package tumblrgraphmaker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class PostUtil {
	
	public static JsonElement getPosts(String blogName, int runCount) throws JsonIOException, JsonSyntaxException, IOException {
		
		return getPosts(blogName, runCount, 1);
	}
	
	public static JsonElement getPosts(String blogName, int postCount, int iteration) throws JsonIOException, JsonSyntaxException, IOException {
		if(iteration > Constants.maxPostLoop){
			return null;
		}
		
		try{
			URL url = new URL(String.format(Constants.posts, blogName, Constants.authKey, postCount));
			return new JsonParser().parse(new InputStreamReader(url.openStream()));
		} catch(FileNotFoundException e){
			//404 error, blog likely deactivated
			System.out.println();
			System.out.println(String.format("FileNotFound exception on blog %s, likely deactivated", blogName));
			
			return null;
			
		}catch (Exception e){
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println(String.format(Constants.posts, blogName, Constants.authKey, postCount));
			return getPosts(blogName, postCount);
		}
		
	}

	public static void printStatus(int count){
		System.out.println();
		System.out.print(String.format("%S: Currently retrieved %d posts.", Constants.fmt.format(Calendar.getInstance().getTime()), count));
	}
	
	public static void printStatus(String preface, int count){
		System.out.println();
		System.out.print(String.format("%S: %s - Currently retrieved %d posts.", Constants.fmt.format(Calendar.getInstance().getTime()), preface, count));
	}

}

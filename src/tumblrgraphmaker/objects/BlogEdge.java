package tumblrgraphmaker.objects;

public class BlogEdge {

	String source;
	String destination;
	public BlogEdge(String source, String destination){
		this.source = source;
		this.destination = destination;
	}
	
	@Override
	public int hashCode(){
		return (source+destination).hashCode();
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof BlogEdge){
			BlogEdge obj2 = (BlogEdge)obj;
			return source.equals(obj2.getSource()) && destination.equals(obj2.getDestination());
		} else {
			return false;
		}
	}
	
	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}

}

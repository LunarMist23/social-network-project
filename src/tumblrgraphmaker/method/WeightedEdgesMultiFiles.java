package tumblrgraphmaker.method;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import tumblrgraphmaker.Constants;
import tumblrgraphmaker.PostUtil;
import tumblrgraphmaker.objects.BlogEdge;
import tumblrgraphmaker.printer.GraphMLMaker;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class WeightedEdgesMultiFiles {

	private static String seed="";
	private static int depth;
	private static int yearRestrict;
	private static Date oldestDate;
	private static GraphMLMaker maker;
	private static String timestamp;

	private static Set<String> evaluatedBlogs;
	private static Set<String> currentBlogSet;
	private static Set<String> nextBlogSet;
	
	public static void run(String s, int d, int y, boolean flag) throws IOException, JsonIOException, JsonSyntaxException, ParseException{

		seed = s;
		depth = d;
		yearRestrict = y;
		
		//currently, flag does nothing in this method
		
		timestamp = Constants.timestamp.format(Calendar.getInstance().getTime());
		
		(new File(timestamp)).mkdir();
		
		{
			Calendar cal = Calendar.getInstance();
			cal.roll(Calendar.YEAR, yearRestrict*-1);
			oldestDate = cal.getTime();
		}
		
		
		
		evaluatedBlogs = new TreeSet<String>();
		currentBlogSet = new TreeSet<String>();
		currentBlogSet.add(seed);
		nextBlogSet = new TreeSet<String>();
		
		for(int cycleCount = 0; cycleCount < depth; cycleCount++){
			System.out.println(String.format("Beginning cycle %d", cycleCount + 1));
			
			currentBlogSet.removeAll(evaluatedBlogs);
			evaluateCurrentBlogSet();
			
			currentBlogSet.clear();
			currentBlogSet.addAll(nextBlogSet);
			nextBlogSet.clear();
		}
		
		System.out.println("Complete!");
		
	}
	
	private static void evaluateCurrentBlogSet() throws JsonIOException, JsonSyntaxException, IOException, ParseException {
		for(String blogToEvaluate : currentBlogSet){
			
			evaluateBlog(blogToEvaluate);
			
			evaluatedBlogs.add(blogToEvaluate);
		}
		
	}


	private static void evaluateBlog(String blogToEvaluate) throws JsonIOException, JsonSyntaxException, IOException, ParseException {
		HashMap<BlogEdge, Integer> edges = new HashMap<BlogEdge, Integer>();
		
		maker = new GraphMLMaker(String.format("%s/%s-%dyr", timestamp, blogToEvaluate, yearRestrict));
		maker.openKey("label", "node", "label", "string");
		maker.closeKey();
		maker.openKey("edgeid", "edge", "Edge", "string");
		maker.closeKey();
		maker.openKey("weight", "edge", "EdgeWeight", "int");
		maker.closeKey();
		maker.openGraph("directed");
		maker.openNode(seed);
		maker.writeData("label", seed);
		maker.closeNode();
		
		System.out.println(String.format("Evaluating:%s", blogToEvaluate));
		
		JsonArray posts = new JsonArray();
		int numberOfPosts = 1;
		int postCount;
		String reblogger;
		String postID;
		Date oldestRetrievedPost = Calendar.getInstance().getTime();
		
		for(postCount = 0; postCount < numberOfPosts && oldestRetrievedPost.after(oldestDate); postCount += 20){
			JsonElement response = PostUtil.getPosts(blogToEvaluate, postCount);
			
			if(response == null){
				return;
			}
			
			if(numberOfPosts >= 1){
				numberOfPosts = response.getAsJsonObject().getAsJsonObject("response").getAsJsonPrimitive("total_posts").getAsInt();
			}
			
			posts.addAll(response.getAsJsonObject().getAsJsonObject("response").getAsJsonArray("posts"));
			String dateString = posts.get(posts.size()-1).getAsJsonObject().getAsJsonPrimitive("date").getAsString();
			oldestRetrievedPost = Constants.fmt.parse(dateString);

			if(postCount % Constants.postUpdateIncrement == 0){
				PostUtil.printStatus(postCount);
			}
		}
		
		System.out.println(String.format("%d posts retrieved", postCount));
		
		for(JsonElement post : posts){
			if(post.getAsJsonObject().has("reblogged_from_name")){
				try{
					reblogger = post.getAsJsonObject().getAsJsonPrimitive("reblogged_from_name").getAsString();
					postID = post.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
					
					if( !(evaluatedBlogs.contains(reblogger) || currentBlogSet.contains(reblogger) || nextBlogSet.contains(reblogger)) ){
						maker.openNode(reblogger);
						maker.writeData("label", reblogger);
						maker.closeNode();
						
						if(reblogger.matches("(.)*-deactivated[0-9]{0,8}")){
							evaluatedBlogs.add(reblogger);
						} else {
							nextBlogSet.add(reblogger);
						}					
					}
					
					
					
					BlogEdge edge = new BlogEdge(reblogger, blogToEvaluate);
					if(edges.containsKey(edge)){
						Integer temp = edges.remove(edge);
						edges.put(edge, temp + 1);
					} else {
						edges.put(edge, 1);
					}
				} catch (ClassCastException e) {
					System.out.println(e);
					System.out.println(post.toString());
				}
			}
		}
		
		int counter = 1;
		for(Entry<BlogEdge,Integer> edge : edges.entrySet()){
			
			if(edge.getKey().equals(new BlogEdge("", blogToEvaluate))){
				System.out.println("Found Empty Blog Name");
			} else {
				maker.openEdge("edge"+counter, edge.getKey().getSource(), edge.getKey().getDestination());
				maker.writeData("weight", edge.getValue().toString());
				maker.closeEdge();
				
				counter++;
			}
		}
		
		maker.closeFile();
		
	}

}

package tumblrgraphmaker.method.multithread;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import tumblrgraphmaker.Constants;
import tumblrgraphmaker.PostUtil;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class BlogThreadAENWOF implements Runnable{

		private String blogName;
		Date oldestDate;

		@Override
		public void run() {
			try {
				evaluateBlog(this.blogName);
			} catch (JsonIOException | JsonSyntaxException | IOException
					| ParseException e) {
				// TODO Auto-generated catch block
				System.out.println(this.blogName);
				e.printStackTrace();
			}
		}
		
		public BlogThreadAENWOF(String blogName, Date oldestDate){
			this.blogName = blogName;
			this.oldestDate = oldestDate;
		}
	
		private void evaluateBlog(String blogToEvaluate) throws JsonIOException, JsonSyntaxException, IOException, ParseException {
		System.out.println(String.format("Evaluating:%s", blogToEvaluate));
		
		JsonArray posts = new JsonArray();
		int numberOfPosts = 1;
		int postCount;
		String reblogger;
		String postID;
		Date oldestRetrievedPost = Calendar.getInstance().getTime();
		
		for(postCount = 0; postCount < numberOfPosts && oldestRetrievedPost.after(oldestDate); postCount += 20){
			JsonElement response = PostUtil.getPosts(blogToEvaluate, postCount);
			
			if(response == null){
				return;
			}
			
			if(numberOfPosts >= 1){
				numberOfPosts = response.getAsJsonObject().getAsJsonObject("response").getAsJsonPrimitive("total_posts").getAsInt();
			}
			
			posts.addAll(response.getAsJsonObject().getAsJsonObject("response").getAsJsonArray("posts"));
			String dateString = posts.get(posts.size()-1).getAsJsonObject().getAsJsonPrimitive("date").getAsString();
			synchronized (Constants.fmt) {
				try{
					oldestRetrievedPost = Constants.fmt.parse(dateString);
				} catch (Exception e){
					System.out.println(String.format("\nError with date formatting - '%s', '%s'", dateString, posts.get(posts.size()-1).toString()));
					throw e;
				}
			}			
			
//			if(postCount!=0){
//				if( postCount % 10 == 9){
//					System.out.print("|");
//				} else {
//					System.out.print(".");
//				}
//			}
//			
			if(postCount % Constants.postUpdateIncrement == 0){
				PostUtil.printStatus(blogToEvaluate, postCount);
			}
			
		}
		
		System.out.println(String.format("\n%s - %d posts retrieved", blogToEvaluate,  postCount));
		
		for(JsonElement post : posts){
			boolean newNode = false;
			evalPost:
			if(post.getAsJsonObject().has("reblogged_from_name")){
				try{
					reblogger = post.getAsJsonObject().getAsJsonPrimitive("reblogged_from_name").getAsString();
										
					postID = post.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
					
					synchronized(MultiThreadAENWOF.evaluatedBlogs){
						synchronized(MultiThreadAENWOF.currentBlogSet){
							synchronized(MultiThreadAENWOF.nextBlogSet){
								if( !(MultiThreadAENWOF.evaluatedBlogs.contains(reblogger) || MultiThreadAENWOF.currentBlogSet.contains(reblogger) || MultiThreadAENWOF.nextBlogSet.contains(reblogger)) ){
									newNode = true;
								}
							}
						}
					}
					
					if( newNode ){
						synchronized (MultiThreadAENWOF.maker) {
							MultiThreadAENWOF.maker.openNode(reblogger);
							MultiThreadAENWOF.maker.writeData("label", reblogger);
							MultiThreadAENWOF.maker.closeNode();
						}
						
						if(reblogger.matches("(.)*-deactivated[0-9]{0,8}")){
							synchronized (MultiThreadAENWOF.evaluatedBlogs) {
								MultiThreadAENWOF.evaluatedBlogs.add(reblogger);	
							}
						} else {
							synchronized (MultiThreadAENWOF.nextBlogSet){
								MultiThreadAENWOF.nextBlogSet.add(reblogger);
							}
						}					
					}
					
					if(reblogger != null && !reblogger.equals("")){
						synchronized (MultiThreadAENWOF.maker) {
							MultiThreadAENWOF.maker.openEdge(postID, reblogger, blogToEvaluate);
							MultiThreadAENWOF.maker.writeData("edgeid", postID);
							MultiThreadAENWOF.maker.closeEdge();
						}
					} else {
//						System.out.println("Edge source is empty");
					}
				} catch (ClassCastException e) {
//					System.out.println(e);
//					System.out.println(post.toString());
				}
			}
		}
		
	}
}


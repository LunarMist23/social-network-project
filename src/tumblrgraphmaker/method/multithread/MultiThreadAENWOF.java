package tumblrgraphmaker.method.multithread;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import tumblrgraphmaker.Constants;
import tumblrgraphmaker.printer.GraphMLMaker;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class MultiThreadAENWOF {

	private static String seed="";
	private static int depth;
	private static int yearRestrict;
	private static Date oldestDate;
	public static GraphMLMaker maker;

	public static Set<String> evaluatedBlogs;
	public static Set<String> currentBlogSet;
	public static Set<String> nextBlogSet;
	
	private static boolean finalGeneration, noFinalLeaves;
	
	public static void run(String s, int d, int y) throws IOException, JsonIOException, JsonSyntaxException, ParseException, InterruptedException{

		seed = s;
		depth = d;
		yearRestrict = y;
		
		{
			Calendar cal = Calendar.getInstance();
			cal.roll(Calendar.YEAR, yearRestrict*-1);
			oldestDate = cal.getTime();
		}
		
		String timestamp = Constants.timestamp.format(Calendar.getInstance().getTime()) + "MT";
		
		(new File(timestamp)).mkdir();
		
		maker = new GraphMLMaker(timestamp, seed, depth, yearRestrict);
		maker.openKey("label", "node", "label", "string");
		maker.closeKey();
		maker.openKey("edgeid", "edge", "Edge", "string");
		maker.closeKey();
		maker.openGraph("directed");
		maker.openNode(seed);
		maker.writeData("label", seed);
		maker.closeNode();
		
		evaluatedBlogs = new TreeSet<String>();
		currentBlogSet = new TreeSet<String>();
		currentBlogSet.add(seed);
		nextBlogSet = new TreeSet<String>();
		
		for(int cycleCount = 0; cycleCount < depth; cycleCount++){
			if(cycleCount == depth - 1){
				finalGeneration = true;
			}
			System.out.println(String.format("\nBeginning cycle %d, %d blogs", cycleCount + 1, currentBlogSet.size()));
			
			currentBlogSet.removeAll(evaluatedBlogs);
			evaluateCurrentBlogSet();
			
			currentBlogSet.clear();
			currentBlogSet.addAll(nextBlogSet);
			nextBlogSet.clear();
		}
		
		maker.closeFile();
		System.out.println("\nComplete!");
		
	}
	
	private static void evaluateCurrentBlogSet() throws JsonIOException, JsonSyntaxException, IOException, ParseException, InterruptedException {
		Set<Thread> threads = new HashSet<Thread>();
		Set<Thread> toRun = new HashSet<Thread>();
		System.setProperty("http.maxConnections",Constants.maxThreads+""); 
		for(String blogToEvaluate : currentBlogSet){
			Thread t = new Thread(new BlogThreadAENWOF(blogToEvaluate, oldestDate));
			threads.add(t);
		}
		
		Iterator<Thread> itr  = threads.iterator();
		
		while(itr.hasNext()){
			
			getThreads:
			for(int i = 0; i < Constants.maxThreads; i++){
				if(itr.hasNext()){
					toRun.add(itr.next());
				} else {
					break getThreads;
				}
			}	
		
			for(Thread t : toRun){
				t.start();
			}

			for(Thread t : toRun){
				t.join();
			}	
			
			toRun.clear();
		}
		
		
		evaluatedBlogs.addAll(currentBlogSet);
	}



}

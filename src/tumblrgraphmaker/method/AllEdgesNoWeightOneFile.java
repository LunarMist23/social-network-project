package tumblrgraphmaker.method;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import tumblrgraphmaker.Constants;
import tumblrgraphmaker.PostUtil;
import tumblrgraphmaker.printer.GraphMLMaker;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class AllEdgesNoWeightOneFile {

	private static String seed="";
	private static int depth;
	private static int yearRestrict;
	private static Date oldestDate;
	private static GraphMLMaker maker;
	
	private static boolean finalGeneration = false;
	private static boolean noFinalLeaves = false;

	private static Set<String> evaluatedBlogs;
	private static Set<String> currentBlogSet;
	private static Set<String> nextBlogSet;
	
	public static void run(String s, int d, int y, boolean flag) throws IOException, JsonIOException, JsonSyntaxException, ParseException{

		seed = s;
		depth = d;
		yearRestrict = y;
		noFinalLeaves = flag;
		
		{
			Calendar cal = Calendar.getInstance();
			cal.roll(Calendar.YEAR, yearRestrict*-1);
			oldestDate = cal.getTime();
		}
		
		String timestamp = Constants.timestamp.format(Calendar.getInstance().getTime());
		
		(new File(timestamp)).mkdir();
		
		maker = new GraphMLMaker(timestamp, seed, depth, yearRestrict);
		maker.openKey("label", "node", "label", "string");
		maker.closeKey();
		maker.openKey("edgeid", "edge", "Edge", "string");
		maker.closeKey();
		maker.openGraph("directed");
		maker.openNode(seed);
		maker.writeData("label", seed);
		maker.closeNode();
		
		evaluatedBlogs = new TreeSet<String>();
		currentBlogSet = new TreeSet<String>();
		currentBlogSet.add(seed);
		nextBlogSet = new TreeSet<String>();
		
		for(int cycleCount = 0; cycleCount < depth; cycleCount++){
			if(cycleCount == depth - 1){
				finalGeneration = true;
			}
			System.out.println(String.format("Beginning cycle %d, %d blogs", cycleCount + 1, currentBlogSet.size()));
			
			currentBlogSet.removeAll(evaluatedBlogs);
			evaluateCurrentBlogSet();
			
			currentBlogSet.clear();
			currentBlogSet.addAll(nextBlogSet);
			nextBlogSet.clear();
		}
		
		maker.closeFile();
		System.out.println("Complete!");
		
	}
	
	private static void evaluateCurrentBlogSet() throws JsonIOException, JsonSyntaxException, IOException, ParseException {
		for(String blogToEvaluate : currentBlogSet){
			
			evaluateBlog(blogToEvaluate);
			
			evaluatedBlogs.add(blogToEvaluate);
		}
		
	}


	private static void evaluateBlog(String blogToEvaluate) throws JsonIOException, JsonSyntaxException, IOException, ParseException {
		System.out.println(String.format("Evaluating:%s", blogToEvaluate));
		
		JsonArray posts = new JsonArray();
		int numberOfPosts = 1;
		int postCount;
		String reblogger;
		String postID;
		Date oldestRetrievedPost = Calendar.getInstance().getTime();
		
		for(postCount = 0; postCount < numberOfPosts && oldestRetrievedPost.after(oldestDate); postCount += 20){
			JsonElement response = PostUtil.getPosts(blogToEvaluate, postCount);
			
			if(response == null){
				return;
			}
			
			if(numberOfPosts >= 1){
				numberOfPosts = response.getAsJsonObject().getAsJsonObject("response").getAsJsonPrimitive("total_posts").getAsInt();
			}
			
			posts.addAll(response.getAsJsonObject().getAsJsonObject("response").getAsJsonArray("posts"));
			String dateString = posts.get(posts.size()-1).getAsJsonObject().getAsJsonPrimitive("date").getAsString();
			oldestRetrievedPost = Constants.fmt.parse(dateString);
			
			if(postCount!=0){
				if( postCount % 10 == 9){
					System.out.print("|");
				} else {
					System.out.print(".");
				}
			}
			
			if(postCount % Constants.postUpdateIncrement == 0){
				PostUtil.printStatus(postCount);
			}
			
		}
		
		System.out.println(String.format("\n%d posts retrieved", postCount));
		
		for(JsonElement post : posts){
			evalPost:
			if(post.getAsJsonObject().has("reblogged_from_name")){
				try{
					reblogger = post.getAsJsonObject().getAsJsonPrimitive("reblogged_from_name").getAsString();
					
					//if it's the final cycle and noFinalLeaves are true, only mark edges where the source node is already a part of the graph, to prevent loose leaves
					if(finalGeneration && noFinalLeaves){
						if( !(evaluatedBlogs.contains(reblogger) || currentBlogSet.contains(reblogger)) ){
							break evalPost;
						}
					}
					
					postID = post.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
					
					if( !(evaluatedBlogs.contains(reblogger) || currentBlogSet.contains(reblogger) || nextBlogSet.contains(reblogger)) ){
						maker.openNode(reblogger);
						maker.writeData("label", reblogger);
						maker.closeNode();
						
						if(reblogger.matches("(.)*-deactivated[0-9]{0,8}")){
							evaluatedBlogs.add(reblogger);
						} else {
							nextBlogSet.add(reblogger);
						}					
					}
					
					if(reblogger != null && !reblogger.equals("")){
						maker.openEdge(postID, reblogger, blogToEvaluate);
						maker.writeData("edgeid", postID);
						maker.closeEdge();
					} else {
						System.out.println("Edge source is empty");
					}
				} catch (ClassCastException e) {
					System.out.println(e);
					System.out.println(post.toString());
				}
			}
		}
		
	}

}

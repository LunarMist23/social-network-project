package tumblrgraphmaker;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

import tumblrgraphmaker.method.AllEdgesNoWeightOneFile;
import tumblrgraphmaker.method.WeightedEdgesMultiFiles;
import tumblrgraphmaker.method.multithread.MultiThreadAENWOF;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class InitialClass {
	private static final int WEIGHTED_MULTI = 1;
	private static final int NONWEIGHTED_SINGLE = 2;
	private static final int NONWEIGHTED_SINGLE_MT = 3;
	
	private static Scanner in = new Scanner(System.in);
	
	
	/**
	 * Main method of the program - will accept up 1 or 3 command line arguments
	 * Argument 1 - String, Tumblr Username of Seed Blog
	 * Argument 2 - Int, Depth of Created Graph
	 * 		Defaults to 2
	 * 		That is to say, will evaluate both the seed user, and all uses the seed has reblogged.
	 * Argument 3 - Int, Number of Years To Restrict Post Age
	 * 		That is to say, with a restriction of X, then only posts that are less than X years old from the current date will be evaluated.
	 * 		May only be set if Depth is also set.
	 * 		Defaults to 1 year
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws JsonSyntaxException 
	 * @throws JsonIOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, JsonIOException, JsonSyntaxException, ParseException, InterruptedException {
		String seed = "";
		int depth, yearRestrict, method;
		boolean noFinalLeaves;
		
		if(args.length >= 1){
			seed = args[0];
		} else {
			System.out.println("Seed blog name not provided. Please input seed blog.");
			seed = in.nextLine().trim();
		}
		
		if(args.length >=2){
			depth = Integer.parseInt(args[1]);
		} else {
			depth = Constants.defaultDepth;
		}

		if(args.length >=3){
			yearRestrict = Integer.parseInt(args[2]);
		} else {
			yearRestrict = Constants.defaultYear;
		}

		if(args.length >=4){
			method = Integer.parseInt(args[3]);
		} else {
			method = NONWEIGHTED_SINGLE_MT;
		}
		
		if(args.length >=5){
			noFinalLeaves = !(Integer.parseInt(args[4]) == 0);//false if and only if 0
		} else {
			noFinalLeaves = false;
		}
		
		if(method == WEIGHTED_MULTI){
			System.out.println("Running Weighted Edge Multi File Method");
			WeightedEdgesMultiFiles.run(seed, depth, yearRestrict, noFinalLeaves);
		} else if(method == NONWEIGHTED_SINGLE){
			System.out.println("Running All Edge Single File Method");
			AllEdgesNoWeightOneFile.run(seed, depth, yearRestrict, noFinalLeaves);
		} else if(method == NONWEIGHTED_SINGLE_MT){
			System.out.println("Running Multi-Threaded All Edge Single File Method");
			MultiThreadAENWOF.run(seed, depth, yearRestrict);
		}
		
	}




}

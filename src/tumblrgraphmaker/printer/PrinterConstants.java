package tumblrgraphmaker.printer;

/**
 * http://graphml.graphdrawing.org/primer/graphml-primer.html
 * @author joyceja
 *
 */
public class PrinterConstants {
	final static String extension = ".graphml";
	
	final static String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
			"<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"\n"+
			"\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"+
			"\txsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns\n"+
			"\t http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n";
	
	/**
	 * Creates a <key> tag, with variable attributes, set with String.format</br>
	 * After use, add <default> data if any, and then remember to closeKey</br>
	 * Attr 1 - String, Key ID - Used as reference in Data tags for setting the value</br>
	 * Attr 2 - String, Object key can refer to, ie. "node", "edge"</br>
	 * Attr 3 - String, Key Name - Different from ID</br>
	 * Attr 4 - String, Attribute Type - ie. "double"
	 */
	final static String openKey = "\t<key id=\"%s\" for=\"%s\" attr.name=\"%s\" attr.type=\"%s\">\n";
	
	/**
	 * Creates a full <default> tag pair</br>
	 * Use String.format with a string parameter to set the default value.</br>
	 * Used inside a key tag
	 */
	final static String defaultTag = "\t\t<default>%s<default/>\n";
	
	final static String closeKey = "\t</key>\n";
	
	/**
	 * Creates the <graph> tag. Use with String.format()</br>
	 * Parameters - string id, "directed"|"undirected"
	 */
	final static String openGraph = "\t<graph id=\"%s\" edgedefault=\"%s\">\n";
	
	/**
	 * Create a node with id %s.
	 * Use String.format
	 */
	final static String openNode = "\t\t<node id=\"%s\">\n";
	
	/**
	 * Create an edge with id of %s1, a source of %s2, and a target of %s3
	 */
	final static String openEdge = "\t\t<edge id=\"%s\" source=\"%s\" target=\"%s\">\n";
	
	final static String closeNode = "\t\t</node>\n";


	public static final String data = "\t\t\t<data key=\"%s\">%s</data>\n";
	
	final static String closeEdge = "\t\t</edge>\n";
	
	final static String closeGraph = "\t</graph>\n";
	
	final static String footer = "</graphml>";

}

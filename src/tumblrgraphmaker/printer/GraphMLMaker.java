package tumblrgraphmaker.printer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GraphMLMaker {
	final String baseName;
	FileWriter writer;
	public int idCountNode = 0;
	public int idCountEdge = 0;
	
	/**
	 * Creates a new instance of a GraphMLMaker for a file with make </br>
	 * <seedName>-depth-<depth>-restrictBy-<yearRestrict>.PrinterConstants.extension
	 * @param seedName
	 * @param depth
	 * @param yearRestrict
	 * @throws IOException
	 */
	public GraphMLMaker(String seedName, int depth, int yearRestrict) throws IOException{
		
		baseName = String.format("%s-depth-%d-restrictBy-%d", seedName, depth, yearRestrict);
		
		File outputFile = new File(baseName + PrinterConstants.extension);
		if(outputFile.exists()){
			//Overwrite, or throw error?
		}
		writer = new FileWriter(outputFile);
		
		writer.write(PrinterConstants.header);
		writer.flush();
	}
	
	public GraphMLMaker(String rootFolder, String seedName, int depth, int yearRestrict) throws IOException{
		
		baseName = String.format("%s/%s-depth-%d-restrictBy-%d", rootFolder, seedName, depth, yearRestrict);
		
		File outputFile = new File(baseName + PrinterConstants.extension);
		if(outputFile.exists()){
			//Overwrite, or throw error?
		}
		writer = new FileWriter(outputFile);
		
		writer.write(PrinterConstants.header);
		writer.flush();
	}

	public GraphMLMaker(String baseFileName) throws IOException{
		
		baseName = baseFileName;
		
		File outputFile = new File(baseName + PrinterConstants.extension);
		if(outputFile.exists()){
			//Overwrite, or throw error?
		}
		writer = new FileWriter(outputFile);
		
		writer.write(PrinterConstants.header);
		writer.flush();
	}
	
	public void openKey(String ID, String For, String AttrName, String AttrType) throws IOException{
		writer.write(
				String.format(
						PrinterConstants.openKey, 
						ID, 
						For, 
						AttrName, 
						AttrType));
		writer.flush();
	}
	
	public void closeKey() throws IOException{
		writer.write(PrinterConstants.closeKey);
		writer.flush();
	}
	
	public void openGraph(String edgeDefault) throws IOException{
		writer.write(
				String.format(
						PrinterConstants.openGraph, 
						"fileName",
						edgeDefault));
		writer.flush();
	}
	
	public void openNode(String ID) throws IOException{
		writer.write(String.format(
						PrinterConstants.openNode,
						ID));
		writer.flush();
	}
	
	public void closeNode() throws IOException{
		writer.write(PrinterConstants.closeNode);
		writer.flush();
	};

	public void writeData(String key, String value) throws IOException {
		writer.write(String.format(PrinterConstants.data,
				key,
				value));
		writer.flush();
	}
	
	public boolean openEdge(String ID, String Source, String Target) throws IOException{
		if(Source == null || Source.equals("") || Target == null || Target.equals("")){
			return false;
		}
		
		writer.write(String.format(
						PrinterConstants.openEdge,
						ID,
						Source,
						Target));
		writer.flush();
		return true;
	}
	
	public void closeEdge() throws IOException{
		writer.write(PrinterConstants.closeEdge);
		writer.flush();
	};
	
	public void closeFile() throws IOException{
		writer.write(PrinterConstants.closeGraph);
		writer.write(PrinterConstants.footer);
		
		writer.close();
	}
}

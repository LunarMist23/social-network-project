package testing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReadDateStringTest {
	//http://stackoverflow.com/questions/26409483/convert-string-to-gmt-timezone-date-java
	public static void main(String[] args) throws ParseException{
		String date = "2014-01-20 20:01:23 GMT";

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
		
		Date dte = fmt.parse(date);
		
		System.out.println();
	}
}

package testing;

import java.io.IOException;

import tumblrgraphmaker.printer.GraphMLMaker;

public class BaseGraphMLTest {

	public static void main(String[] args) throws IOException {
		GraphMLMaker make = new GraphMLMaker("Test", 1, 1);
		
		make.openGraph("directed");
		make.openNode("n1");
		make.closeNode();
		make.openNode("n2");
		make.closeNode();
		make.openNode("n3");
		make.closeNode();
		
		make.openEdge("e1", "n1", "n3");
		make.closeEdge();
		make.openEdge("e2", "n2", "n3");
		make.closeEdge();
		make.openEdge("e3", "n3", "n3");
		make.closeEdge();
		
		make.openNode("n4");
		make.closeNode();
		
		make.openEdge("e4", "n4", "n1");
		make.closeEdge();
		make.openEdge("e5", "n4", "n2");
		make.closeEdge();
		
		make.openNode("n5");
		make.closeNode();
		
		make.openEdge("e6", "n4", "n5");
		make.closeEdge();
		
		make.closeFile();
	}

}

package testing.example;
import java.util.Iterator;


public class CharIterator implements Iterator<Character> {

	private final String str;
	private int index = -1;
	
	
	public CharIterator(String str){
		this.str = str;
	}
	
	@Override
	public boolean hasNext() {
		return index < str.length();
	}

	public Character current() {
		return str.charAt(index);
	}
	
	@Override
	public Character next() {
		index++;
		return str.charAt(index);
	}
	
	public Character peek() {
		return str.charAt(index+1);
	}
	
	public void back(){
		index--;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public String toString() {
		return str.substring(index - 5);
	}

}

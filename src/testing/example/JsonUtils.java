package testing.example;
import java.util.ArrayList;
import java.util.HashMap;


public class JsonUtils {

	public static Object JsonReader(CharIterator itr) throws Exception{
		
		char current = itr.next();
		
		switch(current){
		
		case '{':
			HashMap<String, Object> map = new HashMap();
			if(itr.peek() == '}'){
				current = itr.next();
				return map;
			}
			String key;
			Object obj;
			while(current != '}'){
				key = (String)JsonUtils.JsonReader(itr);
				current = itr.next();
				if(current != ':'){
					throw new Exception();
				}
				obj = JsonUtils.JsonReader(itr);
				map.put(key,  obj);
				current = itr.next();
			}
			
			return map;
		case '[':
			ArrayList<Object> array = new ArrayList();
			if(itr.peek() == ']'){
				current = itr.next();
				return array.toArray();
			}
			while(current != ']'){
				
				array.add(JsonUtils.JsonReader(itr));
				current = itr.next();
				if(current != ',' && current != ']'){
					throw new Exception();
				}
			}
			
			return array.toArray();
		case '"':
			StringBuilder str = new StringBuilder();
			boolean escape = false;
			current = itr.next();
			
			while(current != '"' || escape){
				escape = false;
				str.append(current);
				if(current == '\\'){
					escape = true;
				}
				
				current = itr.next();
			}			
			
			return str.toString();
		default:
			if(Character.isDigit(current)){
				StringBuilder num = new StringBuilder();
				boolean decimal = false;
				
				while(Character.isDigit(current) || current == '.'){
					num.append(current);		
					if(current == '.'){
						decimal = true;
					}		
					current = itr.next();
				}			
				
				itr.back();
				
				if(decimal){
					return Double.parseDouble(num.toString());
				} else {
					return Long.parseLong(num.toString());
				}
			} else {
				//boolean
				if(current == 'f' || current == 'F'){
					itr.next();
					itr.next();
					itr.next();
					itr.next();
					return false;
				} else if(current == 't' || current == 'T'){
					itr.next();
					itr.next();
					itr.next();
					return true;
				} else if(current == 'n' || current == 'N'){
					itr.next();
					itr.next();
					itr.next();
					return null;
				} else {
					throw new Exception();
				}
			}
		}
		
	}
}
